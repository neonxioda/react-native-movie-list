import React, { Component } from 'react';
import { connect } from 'react-redux'
import { StyleSheet, Text, View, Image,ScrollView } from 'react-native';
import store from '../Data/store'
import Axios from 'axios';
console.log(store.getState().Detail);
export default class Details extends Component{
    constructor(props){
        super(props);
        this.state={
            isLoading:true
        }
    }

    componentDidMount() {
        store.dispatch({
            type:'FETCH_REQUEST',
            payload:''
        })
        this.getData()
      }

      async getData (){
        try{
            const data = await Axios.get('http://www.omdbapi.com/?i='+store.getState().Detail+'&apikey=fa52415d');
            store.dispatch({
                    type:'FETCH_SUCCESS',
                    payload:data.data
                })
                this.setState({isLoading:false})
        }catch(err){
            store.dispatch({
                type:'FETCH_FAILED'
            })
            alert(err)
        }
    }

    render(){  
        if(this.state.isLoading==true){
            return(
                <Text style={{textAlign:'center', fontSize:32, textAlign:'center', marginTop:200}}>tunggu</Text>
            );
            }else if (this.state.isLoading==false){
        return(
            <View style={styles.container}>
                   <ScrollView>
                <Text style={styles.welcome}>Selamat Datang :</Text>
                <Text style={styles.welcomeName}>{store.getState().username}</Text>
                <Text style={styles.title}>Detail Film</Text>
                   <Text style={styles.titleFilm}>{store.getState().newsData.Title}</Text>
                <Image style={styles.gambar} source={{uri:store.getState().newsData.Poster}} />
                <View style={styles.item}>
                    <View style={styles.itemBox}>
                        <View style={styles.itemData}>
                            <Text style={styles.itemKey}>Title</Text>
                            <Text style={styles.itemValue}>{store.getState().newsData.Title}</Text>
                        </View>
                        <View style={styles.itemBatas} />
                    </View>
                    <View style={styles.itemBox}>
                        <View style={styles.itemData}>
                            <Text style={styles.itemKey}>Released</Text>
                            <Text style={styles.itemValue}>{store.getState().newsData.Released}</Text>
                        </View>
                        <View style={styles.itemBatas} />
                    </View>
                    <View style={styles.itemBox}>
                        <View style={styles.itemData}>
                            <Text style={styles.itemKey}>Runtime</Text>
                            <Text style={styles.itemValue}>{store.getState().newsData.Runtime}</Text>
                        </View>
                        <View style={styles.itemBatas} />
                    </View>
                    <View style={styles.itemBox}>
                        <View style={styles.itemData}>
                            <Text style={styles.itemKey}>Genre</Text>
                            <Text style={styles.itemValue}>{store.getState().newsData.Genre}</Text>
                        </View>
                        <View style={styles.itemBatas} />
                    </View>
                    <View style={styles.itemBox}>
                        <View style={styles.itemData}>
                            <Text style={styles.itemKey}>Actors</Text>
                            <Text style={styles.itemValue}>{store.getState().newsData.Actors}</Text>
                        </View>
                        <View style={styles.itemBatas} />
                    </View>
                    <View style={styles.itemBox}>
                        <View style={styles.itemData}>
                            <Text style={styles.itemKey}>Writer</Text>
                            <Text style={styles.itemValue}>{store.getState().newsData.Writer}</Text>
                        </View>
                        <View style={styles.itemBatas} />
                    </View>
                    <View style={styles.itemBox}>
                        <View style={styles.itemData}>
                            <Text style={styles.itemKey}>Language</Text>
                            <Text style={styles.itemValue}>{store.getState().newsData.Language}</Text>
                        </View>
                        <View style={styles.itemBatas} />
                    </View>
                    <View style={styles.itemBox}>
                        <View style={styles.itemData}>
                            <Text style={styles.itemKey}>Metascore</Text>
                            <Text style={styles.itemValue}>{store.getState().newsData.Metascore}</Text>
                        </View>
                        <View style={styles.itemBatas} />
                    </View>
                    <View style={styles.itemBox}>
                        <View style={styles.itemData}>
                            <Text style={styles.itemKey}>Ratings</Text>
                            <Text style={styles.itemValue}>{store.getState().newsData.Ratings[0].Value}</Text>
                        </View>
                        <View style={styles.itemBatas} />
                    </View>
                    <View style={styles.itemBox}>
                        <View style={styles.itemData}>
                            <Text style={styles.itemKey}>Plot</Text>
                            <Text style={styles.itemValue}>{store.getState().newsData.Plot}</Text>
                        </View>
                        <View style={styles.itemBatas} />
                    </View>
                </View>
                   </ScrollView>
           </View>
        )
    }
}
}

const styles = StyleSheet.create({
container:{
    backgroundColor:'white',
},welcome:{
    fontSize:18,
    fontWeight:'bold',
    marginLeft:15,
    color:'#f84393'
},welcomeName:{
    fontSize:16,
    marginLeft:15,
},title:{
    fontSize:23,
    textAlign:'center',
    fontWeight:'bold',
    marginTop:25,
    color:'#003366'
},titleFilm:{
    fontSize:23,
    textAlign:'center',
    fontWeight:'bold',
    marginTop:25,
    marginBottom:10,
    color:'#f84393'
},gambar:{
    width:300,
    height:400,
    alignSelf:'center',
    borderRadius:15,
    marginVertical:10
},item:{
    marginHorizontal:25
},itemBox:{
    marginTop:18
},itemKey:{
    color:'#666',
    fontSize:12,
    textTransform:'uppercase',
},itemValue:{
    fontSize:16,
},itemBatas:{
    width:'100%',
    height:0.5,
    backgroundColor:'#F84393',
    marginTop:8
}
});