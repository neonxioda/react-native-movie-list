import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createStackNavigator } from "@react-navigation/stack";
import{ createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import 'react-native-gesture-handler';
import Home from './Home/home';
import Login from'./Login/Login';
import About from'./About/about';
import Details from'./Details/details';
const Stack = createStackNavigator();
const TabStack = createBottomTabNavigator();


export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      title:''
    }
  }
  render() {
    const TabsBottom=()=> (
      <TabStack.Navigator tabBarOptions={{
        activeTintColor: '#f84393',
      }} >
        <TabStack.Screen name="Home" component={Home} color='red' options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color='pink'}) => (
            <MaterialCommunityIcons name="home" color={color} size={25} />
          )}}
          />
        <TabStack.Screen name="About Me" component={About} options={{
          tabBarLabel: 'About Me',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="account" color={color} size={25} />
          )}} />
      </TabStack.Navigator>
    )

    return (
      <NavigationContainer>
        <Stack.Navigator >
        <Stack.Screen name='Login' component={Login} />
        <Stack.Screen name='Home' options={{title:this.state.title}} component={TabsBottom} />
        <Stack.Screen name='Details' component={Details} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

