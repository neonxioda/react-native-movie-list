import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, FlatList } from 'react-native';
import store from '../Data/store'
import Axios from 'axios';

export default class Home extends Component{
    constructor(props){
        super(props);
        this.state={
            isLoading:true,
            cari:'naruto',
            key:false,
        }
    }

    componentDidMount() {
        store.dispatch({
            type:'FETCH_REQUEST',
            payload:''
        })
        this.searchData()
      }

   async searchData (){
        try{
            const data = await Axios.get('http://www.omdbapi.com/?s='+store.getState().Detail+'&apikey=fa52415d');
            store.dispatch({
                    type:'FETCH_SUCCESS',
                    payload:data.data.Search
                })
                this.setState({isLoading:false,key:false})
                
                // console.log(store.getState().newsData.Search)
        }catch(err){
            store.dispatch({
                type:'FETCH_FAILED'
            })
            alert(err)
        }
    }

   detailItem (id){

       store.dispatch({
           type:'Detail',
           payload:id
        })
        this.props.navigation.navigate('Details');

    }

    componentDidUpdate(key){
        if(this.state.key == true){
            this.searchData();

        }
    }

    cariFilm(){
        store.dispatch({
            type:'Detail',
            payload:this.state.cari
         })
         this.setState({key:true})
    }

    render(){        
        if(this.state.isLoading==true){
        return(
            <Text style={{textAlign:'center', fontSize:32, textAlign:'center', marginTop:200}}>tunggu</Text>
        );
        }else if (this.state.isLoading==false){
        return(
            <View style={styles.container}>
                <Text style={styles.welcome}>Selamat Datang :</Text>
                <Text style={styles.welcomeName}>{store.getState().username}</Text>
                <Text style={styles.title}>Aplikasi Daftar Film</Text>
                <View style={styles.cari}>
                <TextInput style={styles.searchItem} onChangeText={(cari)=> this.setState({cari})} placeholder='Cari film ...' />
                <TouchableOpacity style={styles.tombolCari} onPress={()=> this.cariFilm()}>
                       <Text style={styles.judulTombolCari}>Cari</Text>      
                     </TouchableOpacity>
                </View>
                <FlatList data={store.getState().newsData} renderItem={(data)=><ListItem detailItem={this.detailItem.bind(this)}  data={data.item} />}  keyExtractor={(item)=>item.imdbID} />
           </View>

        )
        }
    }
}


class ListItem extends Home{
    render() {
        const data = this.props.data
      return (
        <View style={styles.itemContainer}>       
            <View style={styles.itemBox}>
                <Image source={{uri:data.Poster}} style={styles.itemPoster} />
                <View style={styles.itemBoxData}>
                    <Text style={styles.itemTitle}>{data.Title}</Text>
                    <Text style={styles.itemID}>ID : {data.imdbID}</Text>
                    <Text style={styles.itemYear}>YEAR : {data.Year}</Text>
                    <Text style={styles.itemType}>TYPE : {data.Type}</Text>
                    <TouchableOpacity style={styles.tombol} onPress={()=> this.props.detailItem(data.imdbID)} >
                       <Text style={styles.judulTombol}>Details</Text>      
                     </TouchableOpacity>
                </View>
            </View>
        </View>
      )
    }
  };

const styles = StyleSheet.create({
    container:{
        backgroundColor:'white',
    },welcome:{
        fontSize:18,
        fontWeight:'bold',
        marginLeft:15,
        color:'#f84393',
    },welcomeName:{
        fontSize:16,
        marginLeft:15
    },title:{
        fontSize:23,
        textAlign:'center',
        fontWeight:'bold',
        marginTop:25,
        color:'#f84393'
    },searchItem:{
        borderColor:'#f84393',
        height:30,
        width:'80%',
        borderWidth:1,
        borderRadius:5,
        padding:5
    },itemContainer:{
        flex:1
    },itemBox:{
        marginBottom:15,
        flexDirection:'row',
        paddingHorizontal:20,
        elevation:1,
        paddingVertical:5,
        backgroundColor:'white',
        marginHorizontal:10,
        borderRadius:10,
        paddingVertical:5,
    },itemBoxData:{
        fontSize:18,
        marginLeft:15,
        width:200,
        overflow:'scroll'
    },itemTitle:{
        marginBottom:15,
        fontSize:20,
        marginTop:10,
    },itemPoster:{
        width:120,
        height:200,
        borderRadius:10
    },itemID:{
        fontSize:14,
        fontWeight:'bold'
    },itemType:{
        fontSize:12
    },itemYear:{
        fontSize:12
    },tombol:{
        width:70,
        height:30,
        alignItems:'center',
        backgroundColor:'#f84393',
        padding:7,
        borderRadius:10,
        marginTop:20
    },
    judulTombol:{
       color:'white',
       fontSize:12,
       fontWeight:'bold',
    },tombolCari:{
        width:70,
        height:30,
        alignItems:'center',
        backgroundColor:'#f84393',
        padding:6,
        borderRadius:5,
    },
    judulTombolCari:{
       color:'white',
       fontSize:12,
       fontWeight:'bold',
    },cari:{
        flexDirection:'row',
        marginHorizontal:15,
        marginVertical:5
    }
    
});