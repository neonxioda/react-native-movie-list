import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'; 
import Icons from 'react-native-vector-icons/MaterialCommunityIcons'; 

export default class AboutScreen extends Component{
    render(){
        return (
            <View style={styles.container}>
                    <View style={styles.background}>
                        <Image source={{uri:'https://avatars3.githubusercontent.com/u/61444523?s=460&u=ca889ec007e1fde7dc3935dfb14692a69e14e3a6&v=4'}}
                        style={styles.titleImage}/>
                        <Text style={styles.titleName}>Agung Budi Miantoro</Text>
                    </View>
                        <View style={styles.titleBox}>
                            <Text style={styles.title}>About Me</Text>
                        </View>
                    <ScrollView style={styles.container}>
                        <View style={styles.aboutMe}>
                            <View style={styles.profile}>
                                <Text style={styles.profileTitle}>Profile</Text>
                                <View style={styles.profileBox}>
                                    <View style={styles.profileData}>
                                        <Text style={styles.profileKey}>Name</Text>
                                        <Text style={styles.profileValue}>Agung Budi Miantoro</Text>
                                    </View>
                                        <View style={styles.profileBatas} />
                                    </View>
                                    <View style={styles.profileBox}>
                                        <View style={styles.profileData}>
                                            <Text style={styles.profileKey}>BirthDay</Text>
                                            <Text style={styles.profileValue}>09 - November - 1999</Text>
                                        </View>
                                            <View style={styles.profileBatas} />
                                    </View>
                                    <View style={styles.profileBox}>
                                        <View style={styles.profileData}>
                                            <Text style={styles.profileKey}>Gender</Text>
                                            <Text style={styles.profileValue}>Male</Text>
                                        </View>
                                            <View style={styles.profileBatas} />
                                    </View>
                                    <View style={styles.profileBox}>
                                        <View style={styles.profileData}>
                                            <Text style={styles.profileKey}>Location</Text>
                                            <Text style={styles.profileValue}>Lubuklinggau, Sumatera Selatan, Indonesia</Text>
                                        </View>
                                            <View style={styles.profileBatas} />
                                </View>
                            </View>
                            <View style={styles.contact}>
                                <Text style={styles.profileTitle}>Contact Me</Text>
                                <View style={styles.contactBox}>
                                    <View style={styles.contactData}>
                                        <Icons name='phone' style={{color:'#000', marginLeft:8}} size={40}  />
                                        <Text style={styles.contactDataValue} >085216145705</Text>
                                    </View>
                                </View>
                                <View style={styles.contactBox}>
                                    <View style={styles.contactData}>
                                        <Icons name='whatsapp' style={{color:'#4FCE5D', marginLeft:8}} size={40}  />
                                        <Text style={styles.contactDataValue} >085216145705</Text>
                                    </View>
                                </View>
                                <View style={styles.contactBox}>
                                    <View style={styles.contactData}>
                                        <Icons name='gmail' style={{color:'#D44638', marginLeft:8}} size={40}  />
                                        <Text style={styles.contactDataValue} >agung.miantoro@gmail.com</Text>
                                    </View>
                                </View>
                                <View style={styles.contactBox}>
                                    <View style={styles.contactData}>
                                        <Icon name='logo-facebook' style={{color:'#3b5998', marginLeft:10}} size={40}  />
                                        <Text style={styles.contactDataValue} >https://www.facebook.com/agung.miantoro</Text>
                                    </View>
                                </View>
                                <View style={styles.contactBox}>
                                    <View style={styles.contactData}>
                                        <Icon name='logo-instagram' style={{color:'#f84393', marginLeft:10}} size={40}  />
                                        <Text style={styles.contactDataValue} >@agung_budi_m</Text>
                                    </View>
                                </View>
                                <View style={styles.contactBox}>
                                    <View style={styles.contactData}>
                                        <Icon name='logo-github' style={{color:'#000', marginLeft:10}} size={40}  />
                                        <Text style={styles.contactDataValue} >agungbudimiantoro</Text>
                                    </View>
                                </View>
                                <View style={styles.contactBox}>
                                    <View style={styles.contactData}>
                                        <Icons name='gitlab' style={{color:'#FC6D27', marginLeft:8}} size={40}  />
                                        <Text style={styles.contactDataValue} >@neonxioda</Text>
                                    </View>
                                </View>
                                <View style={styles.contactBox}>
                                    <View style={styles.contactData}>
                                        <Icons name='telegram' style={{color:'#6CC1E3', marginLeft:8}} size={40}  />
                                        <Text style={styles.contactDataValue} >@neonxioda</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white'
    },background:{
        width:400,
        height:260,
        backgroundColor:'#f84393',
    },titleImage:{
        width:100,
        height:100,
        alignSelf:'center',
        marginTop:30,
        borderRadius:100,
        borderColor:'white',
        borderWidth:2
    },titleName:{
        textAlign:'center',
        marginTop:20,
        color:'white',
        fontSize:32,
        fontWeight:'bold'
    },titleBox:{
        alignSelf:'center',
        backgroundColor:'white',
        width:150,
        height:50,
        marginTop:-30,
        shadowColor:'black',
        elevation:3,
        borderColor:'#999999',
        borderWidth:1,
        borderRadius:25
    },title:{
        textAlign:'center',
        marginTop:13,
        color:'#F84349',
        fontSize:16,
    },aboutMe:{
        marginHorizontal:50,
        marginTop:25
    },profileTitle:{
        fontSize:23,
        color:'#f84393',
        fontWeight:'bold'
    },profileBox:{
        marginTop:18
    },profileKey:{
        color:'#666',
        fontSize:12,
        textTransform:'uppercase',
    },profileValue:{
        fontSize:16,
    },profileBatas:{
        width:'100%',
        height:0.5,
        backgroundColor:'#003366',
        marginTop:8
    },contact:{
        marginTop:50,
        flex:1
    },contactBox:{
        marginTop:25,
        flex:1
    },contactData:{
        flexDirection:'row'
    },contactDataValue:{
        fontSize:16,
        marginLeft:30,
        color:'#f84393',
        marginTop:8
    }
  });