  
 export const Reducer = (state, action) => {
    if (action.type == 'setUsername') {
      return { username: action.payload,
        Detail:state.Detail }
    }else if (action.type == 'FETCH_REQUEST'){
      return{
        username:state.username,
        isLoading:true,
        Detail:state.Detail
      }
    }else if (action.type == 'FETCH_SUCCESS'){
      return{
        username:state.username,
        isLoading:false,
        isFailed:false,
        newsData:action.payload,
        Detail:state.Detail
      }
    }else if (action.type == 'FETCH_FAILED'){
      return{
        username:state.username,
        isLoading:false,
        isFailed:true,
        Detail:state.Detail
      }
    }else if (action.type == 'Detail'){
      return{
        username:state.username,
        Detail:action.payload
      }
    }
    return state
  }
