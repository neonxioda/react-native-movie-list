import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'; 
import store from '../Data/store'
import { createStore } from 'redux';

const setUsername = (nr)=> {
    return{
        type: 'setUsername',
        payload:nr
    }
  }

 
export default class Login extends Component{
    constructor(props) {
        super(props)
        this.state = {
            username : '',
            password : '',
          isError: false,
        }
      }

    LoginHandler ()  {
        if(this.state.username == '' || this.state.password == ''){
            this.setState({isError:true})
        }else{
            this.setState({isError:false})
            store.dispatch(setUsername(this.state.username));
            this.props.navigation.navigate('Home');
        }
      }

      

    render(){
        return(
            <View style={styles.container}>
                <Image style={styles.gambar} source={{uri: 'https://images.unsplash.com/photo-1536440136628-849c177e76a1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=625&q=80'}} />
                <Text style={styles.judul}>Login</Text>
                <View style={styles.login}>
                    <Text style={styles.loginJudul}>Username</Text>
                    <TextInput style={styles.loginInput} onChangeText={username => this.setState({username})} />
                    <Text style={styles.loginJudul}>Password</Text>
                    <TextInput style={styles.loginInput} secureTextEntry={true} onChangeText={password => this.setState({password})} />
                </View>
                    <Text style={ this.state.isError ? styles.pesanError : {color:'transparent'} }>Username atau Password belum diisi</Text>
                <TouchableOpacity style={styles.tombol} onPress={()=> this.LoginHandler()}>
                    <Text style={styles.judulTombol}>Login</Text>      
                </TouchableOpacity>
                <View style={styles.alternatifLogin}>
                    <Text style= {styles.alternatifLoginJudul}>Or Login With</Text>
                    <TouchableOpacity>
                    <Icon name='logo-facebook' color='#3b5998' size={35} style={styles.alternatifLoginLogo} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                    <Icon name='logo-twitter' color='#1DA1F2' size={35} style={styles.alternatifLoginLogo} />
                    </TouchableOpacity>
                </View>
           </View>
        )
    }
}

const styles = StyleSheet.create({
container:{
},
gambar:{
    width:'100%',
    height:200,
    borderBottomLeftRadius:25,
    borderBottomRightRadius:25,
    marginBottom:25
},
judul:{
    color:'#003366',
    fontWeight:'bold',
    fontSize:32,
    marginBottom:20,
    textAlign:'center'
},
login :{
    margin:15,

},
loginJudul:{
    fontSize:16,
    marginTop:5,
    color:'#003366'
},
loginInput:{
    width:'100%',
    height:50,
    borderColor:'#003366',
    borderWidth:1,
    marginVertical:5
},
tombol:{
    width:300,
    height:50,
    alignItems:'center',
    backgroundColor:'#f84393',
    alignSelf:'center',
    padding:8,
    borderRadius:25,
    marginTop:25,
    marginBottom:30
},
judulTombol:{
   color:'white',
   fontSize:23,
   fontWeight:'bold',
},
alternatifLogin:{
    textAlign:'center',
    flexDirection:'row',
    justifyContent:'center'
},
alternatifLoginJudul:{
    paddingTop:8
},
alternatifLoginLogo:{
    marginHorizontal:10,
},pesanError:{
    textAlign:'center',
    color:'red'
}

});